// Ryan Appel

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit { SPADE, HEART, CLUB, DIAMOND };

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	Card c;
	c.rank = NINE;
	c.suit = CLUB;

	Card c2;
	c2.rank = TEN;
	c2.suit = SPADE;

	// for testing...
	//PrintCard(HighCard(c, c2));

	_getch();
	return 0;
}

